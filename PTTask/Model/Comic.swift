//
//  Comic.swift
//  PTTask
//
//  Created by Amr on 3/23/21.
//
import Foundation

struct ComicsResponse: Codable {
    let code: Int?
    let status, copyright, attributionText, attributionHTML: String?
    let etag: String?
    let data: DataClass?
}

// MARK: - DataClass
struct DataClass: Codable {
    let offset, limit, total, count: Int?
    let comics: [Comic]?
    enum CodingKeys: String, CodingKey {
        case comics = "results"
        case offset, limit, total, count
    }
}

// MARK: - Result
struct Comic: Codable {
    let id: Int?
    let title: String?
    var price: Int {
        Int.random(in: 0..<1000)
    }
    let thumbnail: Thumbnail?

    enum CodingKeys: String, CodingKey {
        case id
        case title, thumbnail
    }
}

// MARK: - Thumbnail
struct Thumbnail: Codable {
    let path: String?
    let imageExtension: String?

    var thumbnailURL: URL? {
        guard let thumnailPath = path, let thumbnailExtension = imageExtension else { return nil }
        let securedPath = thumnailPath.replacingOccurrences(of: "http", with: "https")
        return URL(string: "\(securedPath).\(thumbnailExtension)")
    }

    enum CodingKeys: String, CodingKey {
        case path
        case imageExtension = "extension"
    }
}
