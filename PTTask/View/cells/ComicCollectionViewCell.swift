//
//  ComicCollectionViewCell.swift
//  PTTask
//
//  Created by Amr on 3/24/21.
//

import UIKit
import Kingfisher

class ComicCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var comicPriceLabel: UILabel!
    @IBOutlet weak var comicTitleLabel: UILabel!
    @IBOutlet weak var comicImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configureCell(with comic: Comic) {
        if let thumbnailURL = comic.thumbnail?.thumbnailURL {
            comicImageView.kf.setImage(with: thumbnailURL)
        }
        comicPriceLabel.text = "\(comic.price) AED"
        comicTitleLabel.text = comic.title
    }

}
