//
//  BaseViewController.swift
//  PTTask
//
//  Created by Amr on 3/24/21.
//

import UIKit
import PKHUD

enum UIState: Equatable {
    case showLoading
    case dismissLoading
    case showSuccess(message: String?)
    case showError(error: String?)
}

class BaseViewController: UIViewController {
    func showLoading() {
        PKHUD.sharedHUD.contentView = PKHUDProgressView()
        PKHUD.sharedHUD.show()
    }

    func dismissLoading() {
        PKHUD.sharedHUD.hide()
    }

    func showSuccess(message: String? = nil,
                     hideAfter interval: TimeInterval = 1.0) {
        PKHUD.sharedHUD.contentView = PKHUDSuccessView(title: "success", subtitle: message)
        PKHUD.sharedHUD.show()
        PKHUD.sharedHUD.hide(afterDelay: interval)
    }

    func showError(message: String? = nil,
                     hideAfter interval: TimeInterval = 1.0) {
        PKHUD.sharedHUD.contentView = PKHUDErrorView(title: "error", subtitle: message)
        PKHUD.sharedHUD.show()
        PKHUD.sharedHUD.hide(afterDelay: interval)
    }
}
