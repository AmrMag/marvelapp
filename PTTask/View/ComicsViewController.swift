//
//  ViewController.swift
//  PTTask
//
//  Created by Amr on 3/21/21.
//

import UIKit
import RxCocoa


class ComicsViewController: BaseViewController {
    let viewModel = ComicsViewModel()
    @IBOutlet weak var collectionView: UICollectionView!

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        bindUI()
        viewModel.viewLoaded()
    }

    private func setupUI() {
        setupCollectionView()
    }

    private func bindUI() {
        bindUIState()
        bindCollectionView()
    }

    private func setupCollectionView() {
        collectionView.register(UINib(nibName: "ComicCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ComicCollectionViewCell")
    }

    private func bindUIState() {
        viewModel.uiStateSubject
            .subscribe(onNext: { [weak self] state in
                self?.handleUIState(state: state)
            }).disposed(by: viewModel.disposeBag)

    }

    private func bindCollectionView() {
        collectionView
            .rx
            .setDelegate(self)
            .disposed(by: viewModel.disposeBag)

        viewModel.comicsSubject
            .bind(to: collectionView.rx.items(cellIdentifier: "ComicCollectionViewCell",
                                              cellType: ComicCollectionViewCell.self)) {  _, model, cell in
                cell.configureCell(with: model)
            }
            .disposed(by: viewModel.disposeBag)

        collectionView
            .rx
            .modelSelected(Comic.self)
            .subscribe(onNext: { [weak self] comic in
                self?.presentPayment(for: comic)
            }).disposed(by: viewModel.disposeBag)
    }

    private func handleUIState(state: UIState) {
        switch state {
        case .showLoading:
            self.showLoading()
        case .dismissLoading:
            self.dismissLoading()
        case .showError(let error):
            self.showError(message: error)
        case .showSuccess(let message):
            self.showSuccess(message: message)
        }
    }

    private func presentPayment(for comic: Comic) {
        let theme = PaymentSDKTheme.default
        theme.logoImage = UIImage(named: "marvel")
        
        let configuration = PaymentSDKConfiguration(profileID: constants.profileID,
                                                   serverKey: constants.serverKey,
                                                   clientKey: constants.clientKey,
                                                   cartID: "12345",
                                                   currency: "AED",
                                                   amount: Double(comic.price),
                                                   cartDescription: comic.title ?? "",
                                                   merchantCountryCode: "ae",
                                                   showBillingInfo: true,
                                                   showShippingInfo: true,
                                                   screenTitle: "Pay with Card",
                                                   billingDetails: PaymentSDKBillingDetails(),
                                                   theme: theme)


        PaymentManager.startCardPayment(on: self, configuration: configuration, delegate: viewModel)
    }
}

extension ComicsViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.frame.width / 2) - 10, height: 210)
    }
}


