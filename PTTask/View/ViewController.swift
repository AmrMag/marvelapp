//
//  ViewController.swift
//  PTTask
//
//  Created by Amr on 3/21/21.
//

import UIKit
import RxCocoa


class ComicsViewController: BaseViewController {
    let viewModel = ComicsViewModel()
    @IBOutlet weak var collectionView: UICollectionView!

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        bindCollectionView()
    }

    private func setupUI() {
        setupCollectionView()
    }

    private func setupCollectionView() {
        collectionView.register(UINib(nibName: "ComicCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ComicCollectionViewCell")
    }

    private func bindCollectionView() {
        collectionView
            .rx
            .setDelegate(self)
            .disposed(by: viewModel.disposeBag)

        viewModel.comicsSubject
            .bind(to: collectionView.rx.items(cellIdentifier: "ComicCollectionViewCell",
                cellType: ComicCollectionViewCell.self)) { _, model, cell in
                cell.configureCell(with: model)
        }
        .disposed(by: viewModel.disposeBag)

        collectionView
                .rx
                .modelSelected(Comic.self)
                .subscribe(onNext: { [weak viewModel] (comic) in
                    viewModel?.didSelectComic(comic: comic)
                }).disposed(by: viewModel.disposeBag)
 }



}

extension ComicsViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.frame.width / 2) - 10, height: 210)
    }
}

//extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
//    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        return 20
//    }
//
//    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ComicCollectionViewCell", for: indexPath) as! ComicCollectionViewCell
////        cell.setupWith(presenter.currentItemAt(indexPath.item))
//        return cell
//    }
//
//
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//
//        return CGSize(width: (collectionView.frame.width / 2) - 10, height: 200)
//    }
//
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        let billingDetails = PaymentSDKBillingDetails(name: "John Smith",
//                                                    email: "email@test.com",
//                                                    phone: "+2011111111",
//                                                    addressLine: "address",
//                                                    city: "Dubai",
//                                                   state: "Dubai",
//                                                   countryCode: "ae", // ISO alpha 2
//                                                   zip: "12345")
//
//        let configuration = PaymentSDKConfiguration(profileID: "49611",
//                                            serverKey: "SMJNLTR2G6-JBGNGKBBM9-2MB6HGBG6M",
//                                            clientKey: "CKKMDG-KDD262-TQTK22-GQGMHN",
//                                            cartID: "12345",
//                                            currency: "AED",
//                                            amount: 5.0,
//                                            cartDescription: "Flowers",
//                                            merchantCountryCode: "ae", // ISO alpha 2
//                                            showBillingInfo: false,
//                                            screenTitle: "Pay with Card",
//                                            billingDetails: billingDetails)
//
//        PaymentManager.startCardPayment(on: self, configuration: configuration, delegate: self)
//    }
//
//}
//
extension ComicsViewController: PaymentManagerDelegate {

    func paymentManager(didFinishTransaction transactionDetails: PaymentSDKTransactionDetails?, error: Error?) {
        if let transactionDetails = transactionDetails {
            print("Response Code: " + (transactionDetails.paymentResult?.responseCode ?? ""))
            print("Result: " + (transactionDetails.paymentResult?.responseMessage ?? ""))
            print("Token: " + (transactionDetails.token ?? ""))
            print("Transaction Reference: " + (transactionDetails.transactionReference ?? ""))
            print("Transaction Time: " + (transactionDetails.paymentResult?.transactionTime ?? "" ))
        } else if let error = error {
            print("error \(error)")
//            showError(message: error.localizedDescription)
        }
    }
}
