//
//  APIRouter.swift
//  PTTask
//
//  Created Amr on 3/21/21.
//

import Foundation
import Alamofire

enum APIRouter: URLRequestConvertible {
    
    case getComics
    
    //MARK: - URLRequestConvertible
    func asURLRequest() throws -> URLRequest {
        let url = try NetworkConstants.baseUrl.asURL()
        
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        
        urlRequest.httpMethod = method.rawValue
        
        urlRequest.setValue(NetworkConstants.ContentType.json.rawValue, forHTTPHeaderField: NetworkConstants.HttpHeaderField.acceptType.rawValue)
        urlRequest.setValue(NetworkConstants.ContentType.json.rawValue, forHTTPHeaderField: NetworkConstants.HttpHeaderField.contentType.rawValue)
        
        let encoding: ParameterEncoding = {
            switch method {
            case .get:
                return URLEncoding.default
            default:
                return JSONEncoding.default
            }
        }()
        
        return try encoding.encode(urlRequest, with: parameters)
    }
    
    //MARK: - HttpMethod
    private var method: HTTPMethod {
        switch self {
        case .getComics:
            return .get
        }
    }
    
    //MARK: - Path
    private var path: String {
        switch self {
        case .getComics:
            return "/comics"
        }
    }
    
    //MARK: - Parameters
    private var parameters: Parameters? {
        switch self {
        case .getComics:
            //A dictionary of the key (From the constants file) and its value is returned
            return
                [NetworkConstants.Parameters.timeStamp: 1,
                 NetworkConstants.Parameters.apikey: NetworkConstants.apiKey,
                 NetworkConstants.Parameters.hash: NetworkConstants.hash]
        }
    }
}
