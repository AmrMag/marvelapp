//
//  Constant.swift
//  PTTask
//
//  Created by Amr on 3/21/21.
//

import Foundation
struct NetworkConstants {
    
    static let baseUrl = "https://gateway.marvel.com/v1/public"
    static let apiKey = "e1ab6a4f99a5e0bfe50b041a7a9900c7"
    static let hash = "87b4f64d6a88a9564330339e53a2434b"
    
    struct Parameters {
        static let timeStamp = "ts"
        static let apikey = "apikey"
        static let hash = "hash"
    }
    
    enum HttpHeaderField: String {
        case authentication = "Authorization"
        case contentType = "Content-Type"
        case acceptType = "Accept"
        case acceptEncoding = "Accept-Encoding"
    }
    
    enum ContentType: String {
        case json = "application/json"
    }
}
