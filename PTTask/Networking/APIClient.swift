//
//  APIClient.swift
//  PTTask
//
//  Created by Amr on 3/21/21.
//

import Foundation
import RxSwift
import Alamofire

class APIClient {
    static let sharedInstance = APIClient()
    private init() {}
    
    func getComics() -> Single<ComicsResponse> {
        return requestRemote(APIRouter.getComics)
    }
    
    private func requestRemote(_ urlConvertible: URLRequestConvertible) -> Single<ComicsResponse> {
        return Single<ComicsResponse>.create { observer in
            let request = AF.request(urlConvertible).responseData { (response) in
                switch response.result {
                case .success(let value):
                    do {
                        let comicsDTO = try JSONDecoder().decode(ComicsResponse.self, from: value)
                        observer(.success(comicsDTO))

                    } catch {
                        print(error)
                        observer(.failure(error))
                    }
                case .failure(let error):
                    switch response.response?.statusCode {
                    case 403:
                        observer(.failure(ApiError.forbidden))
                    case 404:
                        observer(.failure(ApiError.notFound))
                    case 409:
                        observer(.failure(ApiError.conflict))
                    case 500:
                        observer(.failure(ApiError.internalServerError))
                    default:
                        observer(.failure(error))
                    }
                }
            }
            return Disposables.create {
                request.cancel()
            }
        }
    }
}
