//
//  APIError.swift
//  PTTask
//
//  Created by Amr on 3/21/21.
//

import Foundation
enum ApiError: Error {
    case forbidden
    case notFound
    case conflict
    case internalServerError    
    case parsingError

    var errorDescription: String? {
        switch self {
        case .forbidden:
            return "Forbidden"
        case .notFound:
            return "Not found"
        default:
            return "Something Went Wrong"
        }
    }
}

