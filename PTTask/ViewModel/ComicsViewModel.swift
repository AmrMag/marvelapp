//
//  ComicsViewModel.swift
//  PTTask
//
//  Created by Amr on 3/24/21.
//

import Foundation
import RxSwift
import PaymentSDK

class ComicsViewModel {
    private let apiClient: APIClient
    let disposeBag: DisposeBag
    private let retrivedComics = PublishSubject<[Comic]>()
    private let currentUIState = PublishSubject<UIState>()

    var comicsSubject: PublishSubject<[Comic]> {
        return retrivedComics
    }
     var uiStateSubject: PublishSubject<UIState> {
        return currentUIState
    }

    init(apiClient: APIClient = APIClient.sharedInstance) {
        self.apiClient = apiClient
        self.disposeBag = DisposeBag()
    }

    func viewLoaded() {
        currentUIState.onNext(.showLoading)
        requestAllComics()
    }

    private func requestAllComics() {
        apiClient.getComics()
            .subscribe { [weak self] (response) in
                self?.currentUIState.onNext(.dismissLoading)
                self?.retrivedComics.onNext(response.data?.comics ?? [])
        } onFailure: { [weak self] (error) in
            self?.currentUIState.onNext(.showError(error: error.localizedDescription))
        }.disposed(by: disposeBag)
    }
}

extension ComicsViewModel: PaymentManagerDelegate {

    func paymentManager(didFinishTransaction transactionDetails: PaymentSDKTransactionDetails?, error: Error?) {
        if transactionDetails != nil {
            self.currentUIState.onNext(.showSuccess(message: "Payment Succeded"))
        } else if let error = error {
            self.currentUIState.onNext(.showError(error: error.localizedDescription))
        }
    }
}
