//  PTTaskTests.swift
//  PTTaskTests
//
//  Created by Amr on 3/21/21.
//

import XCTest
import RxSwift
import RxTest

@testable import PTTask

class PTTaskTests: XCTestCase {
    var sut: ComicsViewModel!
    var scheduler: TestScheduler!
    var uiStateResult: TestableObserver<UIState>!
    var disposeBag: DisposeBag!

    override func setUpWithError() throws {
        sut = ComicsViewModel()
        scheduler = TestScheduler(initialClock: 0)
        uiStateResult = scheduler.createObserver(UIState.self)
        disposeBag = DisposeBag()
    }

    override func tearDownWithError() throws {
        super.tearDown()
        sut = nil
        scheduler = nil
        uiStateResult = nil
        disposeBag = nil
    }

    func testWhenViewStartsThenLoadingIndicatorAppears() {
        sut.uiStateSubject.bind(to: uiStateResult).disposed(by: disposeBag)
        sut.viewLoaded()
        scheduler.start()
        XCTAssertEqual(uiStateResult.events, [.next(0, UIState.showLoading)])
    }

}
